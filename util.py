

def onlyalpha(name):
    return ''.join([x for x in name if x.isalpha()])
