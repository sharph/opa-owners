# opa_owners aka [propertyowners.phl.party](https://propertyowners.phl.party/)

## Requirements

* Python
* nodejs/npm
* wget

## Setup

Run
```bash
bash prep.sh
```
Wait a long time for the data to be downloaded and indexed. Free up some RAM
before you do this. The data are around 1GB but about 9GB in python
objects. ¯\\\_(ツ)\_/¯

This process will write data for the app to consume to:

* opa_owners/src/owners.json
* opa_owners/public/data/

Then:

```bash
cd opa_owners
npm install
```

## Run development

```bash
npm start
```
