FROM debian:stable AS DATA

RUN apt-get update && apt-get -y install python3 wget
RUN mkdir /data
WORKDIR /data
COPY *sh *py /data/
COPY opa_owners /data/opa_owners
RUN PYTHONUNBUFFERED=unbuffered bash prep.sh

from node:14 as BUILD

COPY opa_owners/ /opa_owners
WORKDIR /opa_owners

COPY --from=DATA /data/opa_owners/public/data/* /opa_owners/public/data/
COPY --from=DATA /data/opa_owners/src/owners.json /opa_owners/src/owners.json

RUN npm install
RUN npm run build

FROM nginx:stable-alpine

RUN rm /usr/share/nginx/html/*
COPY --from=BUILD /opa_owners/build/ /usr/share/nginx/html/
COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
