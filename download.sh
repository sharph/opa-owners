#!/bin/bash

wget -q -O opaproperties1.geojson 'https://phl.carto.com/api/v2/sql?filename=opa_properties_public&format=geojson&skipfields=cartodb_id&q=SELECT%20*%20FROM%20opa_properties_public%20%20WHERE%20cartodb_id%20%3C%20300000'
wget -q -O opaproperties2.geojson 'https://phl.carto.com/api/v2/sql?filename=opa_properties_public&format=geojson&skipfields=cartodb_id&q=SELECT%20*%20FROM%20opa_properties_public%20WHERE%20cartodb_id%20%3E=%20300000'
