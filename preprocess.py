import json
import pprint
import util

print('loading...')
with open('opaproperties1.geojson', 'r') as f:
    d1 = json.load(f)

with open('opaproperties2.geojson', 'r') as f:
    d2 = json.load(f)

print('done...')

all = d1['features'] + d2['features']

del d1
del d2

owners = [x['properties']['owner_1'].strip() for x in all]
owners += [x['properties']['owner_2'].strip() for x in all if x['properties']['owner_2'] is not None]

count_owners = {}

for owner in owners:
    if owner not in count_owners:
        count_owners[owner] = 0
    count_owners[owner] += 1

#pprint.pprint(count_owners[:10])

owner_idx = {}

for owner in count_owners.keys():
    for word in owner.split(' '):
        if word == '':
            continue
        prefix = util.onlyalpha(word)[:2].upper()
        if prefix not in owner_idx:
            owner_idx[prefix] = []
        owner_idx[prefix].append(owner)

with open('opa_owners/src/owners.json', 'w') as f:
    f.write(json.dumps(owner_idx, indent=2))


def ismatch(l1, l2, property):
    if len(util.onlyalpha(property['properties']['owner_1'])) > 1 and \
            util.onlyalpha(property['properties']['owner_1'])[:2].upper() == l1 + l2:
        return True
    elif len(util.onlyalpha(property['properties']['owner_1'])) < 2 and \
            l1 == 'Z' and l2 == 'Z':  # dump all the weird matches in ZZ
        return True
    elif property['properties']['owner_2'] is None:
        return False
    elif len(util.onlyalpha(property['properties']['owner_2'])) > 1 and \
            util.onlyalpha(property['properties']['owner_2'])[:2].upper() == l1 + l2:
        return True
    else:
        return False


def writefile(l1, l2):
    print('finding {}{}'.format(l1, l2))
    matches = [x for x in all if ismatch(l1, l2, x)]
    print('writing')
    with open('opa_owners/public/data/{}{}.geojson'.format(l1, l2), 'w') as f:
        f.write(json.dumps({
            "type": "FeatureCollection",
            "features": matches
        }))


for l1 in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
    for l2 in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
        writefile(l1, l2)
